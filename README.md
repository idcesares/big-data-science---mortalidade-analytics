**Sobre o Projeto**

- Consiste no trabalho final da disciplina Big Data Science da Pós-Graduação UFRJ implementando os conceitos aprendidos sobre big data e suas ferramentas de processamento como o Apache Spark.

**Como utilizar este projeto**
- Preencha todos os pré-requisitos que serão mencionados em um servidor linux, altere os parâmetros indicados no código e faça o [download](https://bigdata-metadados.icict.fiocruz.br/dataset/sistema-de-informacoes-de-mortalidade-sim/resource/137051c9-7f8c-4980-9149-b84eba1ff927) da base de dados.

**Pré-requisitos**
- Java JDK 8;
- Apache Spark 2.4.4 e Hadoop 2.7;
- Python 3.7.4;
- PySpark;
- Pandas;
- SQL Alquemy;
- Flask;
- MYSQL Connector https://dev.mysql.com/downloads/connector/net/

**1. Sobre os dados**

*Informações sobre os dados*

Dados do Sistema de Informações de Mortalidade (SIM) reunidos pela Plataforma de Ciência de Dados aplicada à Saúde (PCDaS). Os dados foram obtidos junto ao DATASUS (Ministério da Saúde), tratados e enriquecidos seguindo uma metodologia própria de ETL da PCDaS, resultando em um dataset anual com todos os registros das declarações de óbito contidas no SIM à partir de 1996.

**2. Planejamento**

*O que iremos fazer*

Utilizar os dados provisionados pelo Ministério da Saúde para realizar uma análise exploratória visual, utilizando a ferramenta de processamento de Big Data Spark. O esquema de processamento será o seguinte:
.CSV -> SPARK (PySpark - Spark SQL + Pandas) -> SGBD MYSQL -> GOOGLE DATA STUDIO

**3. Spark**

*O processamento dos dados*


Nesta etapa o Spark processará os dados do arquivo.csv, executará as queries SQL na tabela temporária do dataframe principal e exportará para uma database de um SGBD MySql. Ao clicar no botão Processar dados, o sistema executará todo o processamento necessário. Acompanhe o processo pela console do servidor SPARK/PYTHON. Em Download .csv, você poderá ver o resultado do processamento do 1º dataframe gerado pelas queries Spark SQL.

**4. Data Visualization**

*Visualização do dashboard*

Dashboards construídos a partir da análise das tabelas geradas pelo Spark que foram enviadas para o servidor MySQL conectado ao Google Data Studio