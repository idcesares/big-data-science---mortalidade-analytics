# Dependências da aplicação
from flask import Flask, render_template, request, redirect, flash, url_for, send_file
import pyspark
from pyspark.sql import SparkSession
from pyspark.sql.types import *
import pandas as pd
import mysql.connector
from sqlalchemy import create_engine
import os

# Iniciando aplicativo Flask
app = Flask(__name__)
app.secret_key = 'pos'

# Rota para renderização da página inicial
@app.route('/')
def index():
    return render_template('index.html')

# Rota para processamento do Spark
@app.route('/spark')
def data_load():
    # Criando a sessão local do Spark
    spark = SparkSession.builder.master("local[*]").getOrCreate()
    # Localização do arquivo
    file_path = 'caminho_do_arquivo_ETLSIM.DORES_2016.csv' # Arquivo omitido devido ao tamanho total
    # Lendo o arquivo .csv e criando o Spark Dataframe
    df = spark.read.format("csv").option("header", "true").option("delimiter", "\t").load(file_path).limit(100000) # O arquivo já possui header, inferiremos o esquema automaticamente. Também limitaremos a 100.000 linhas de dados para otimizar o tempo de processamento.
    df.createOrReplaceTempView("mortalidade_data") # cria a tabela temporária que eserá utilizada nas queries
    df.printSchema() # Imprime a estrutura do cabeçalho da tabela
    df.show() # Mostra a estrutura do dataframe

    # Criando novos Dataframe a partir de queries feitas com o Spark Sql
    # Quantidade de mortes por Tipo de Óbito (fetal ou não), Sexo (Masculino; Feminino; Ignorado) e Raça/Cor(Branca; Preta; Amarela; Parda; Indígena). O resultado (DataFrame) deve gerar registros para a tabela Mortes_TipoObitoSexoRacaCor
    df1 = spark.sql('''Select Count(*) as qtd_casos, def_tipo_obito, def_sexo, def_raca_cor From mortalidade_data Group By def_tipo_obito, def_sexo, def_raca_cor''')
    # Mortes por município e UF. O resultado (DataFrame) deve gerar registros para a tabela Mortes_MunicioUF.
    df2 = spark.sql('''Select Count(*) as qtd_casos, res_MUNNOMEX, res_SIGLA_UF From mortalidade_data Group By res_MUNNOMEX, res_SIGLA_UF''')
    # Quantidade de mortes por dia de semana, sexo e raça/cor. O resultado (DataFrame) deve gerar registros para a tabela Mortes_DiaSexoRaca.
    df3 = spark.sql('''Select Count(*) as qtd_casos, dia_semana_obito, def_sexo, def_raca_cor From mortalidade_data Group By dia_semana_obito, def_sexo, def_raca_cor''')
    # Média de idade, por sexo e raça/cor, O resultado (DataFrame) deve gerar registros para a tabela Media_IdadeSexoRaca.
    df4 = spark.sql('''Select AVG(idade_obito_calculado) as media_idade, def_sexo, def_raca_cor From mortalidade_data Where def_tipo_obito <> 'fetal' Group By def_sexo, def_raca_cor''')
    # Quantidade por Causa Básica (conforme a Classificação Internacional de Doença (CID), 10a. Revisão), sexo e raça/cor. O resultado (DataFrame) deve gerar registros para a tabela Causa_BasicaSexoRaca.
    df5 = spark.sql('''Select Count(*) as qtd_casos, CAUSABAS, def_sexo, def_raca_cor From mortalidade_data Group By CAUSABAS, def_sexo, def_raca_cor''') 
    # Mostra o primeiro dataframe gerado para fins de exemplificação
    df1.show()

    # Converter Spark DataFrame para Pandas DataFrame para enviar para MySQL Server | Utilização do Pandas para simplificação do processo de exportação do dataframe para o SGBD
    df1_pandas = df1.toPandas()
    df2_pandas = df2.toPandas()
    df3_pandas = df3.toPandas()
    df4_pandas = df4.toPandas()
    df5_pandas = df5.toPandas()

    # Cria a conexão com o SGBD MySQL e atribui à variável engine
    engine = create_engine('mysql+mysqlconnector://USER:PASSWORD@HOST:3306/DATABASENAME ', echo=False)
    
    # Enviando Pandas Dataframe para Servidor MySQL com o respectivo nome de cada uma das tabelas. Se a tabela já existir, será sobrescrita.
    df1_pandas.to_sql(name='mortes_tipoobitosexoracacor', con=engine, if_exists = 'replace', index=False)
    df2_pandas.to_sql(name='mortes_municiouf', con=engine, if_exists = 'replace', index=False)
    df3_pandas.to_sql(name='mortes_diasexoraca', con=engine, if_exists = 'replace', index=False)
    df4_pandas.to_sql(name='media_idadesexoraca', con=engine, if_exists = 'replace', index=False)
    df5_pandas.to_sql(name='causa_basicasexoraca', con=engine, if_exists = 'replace', index=False)
    
    # Disponibilizando 1º Dataframe em .csv para download de exemplo
    df1.coalesce(1).write.option("delimiter",",").option("header", "true").mode("overwrite").csv('result_mortalidade')
    
    # Alerta de processamento e renderização da página
    flash('Processamento Finalizado! Informações enviadas para o Banco de Dados. Verifique o Dashboard.')
    return render_template('index.html')

# Rota para download do arquivo .CSV de teste
@app.route('/download')
def download():
    path = "caminho_para_o_arquivo_resultado_df1.csv"
    return send_file(path, as_attachment=True)

# Rota para a visualização dos 
@app.route('/datavisualization')
def data_visualization():
    return render_template('datavisualization.html')


app.run(debug=True, host='0.0.0.0', port=80)


